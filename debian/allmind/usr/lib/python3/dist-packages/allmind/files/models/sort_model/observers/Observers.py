# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

# from .EnumerationFinished import DeltaEnumerationFinished
from .Activate import DeltaActivate
from .ShowCurrentDirectory import DeltaShowCurrentDirectory


class EchoObservers:

    def __init__(self, parent):
        # DeltaEnumerationFinished(parent)
        DeltaActivate(parent)
        DeltaShowCurrentDirectory(parent)
