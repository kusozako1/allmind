# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from allmind.Entity import DeltaEntity


class DeltaShowHidden(Gtk.CustomFilter, DeltaEntity):

    def _filter_func(self, file_info, user_data=None):
        return not file_info.get_is_hidden()

    def __init__(self, parent):
        self._parent = parent
        Gtk.CustomFilter.__init__(self)
        self.set_filter_func(self._filter_func)
        self._raise("delta > add filter", self)
