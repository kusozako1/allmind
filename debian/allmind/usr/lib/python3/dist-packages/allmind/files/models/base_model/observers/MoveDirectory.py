# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from allmind.files import Signals
from allmind.alfa.LoopSignalReceiver import AlfaLoopSignalReceiver


class DeltaMoveDirectory(AlfaLoopSignalReceiver):

    __accept_signal__ = Signals.MOVE_DIRECTORY

    def _on_signal_received(self, path):
        if not GLib.file_test(path, GLib.FileTest.EXISTS):
            print("ALLMIND [error] > path not exists :", path)
        else:
            self._raise("delta > move directory", path)
