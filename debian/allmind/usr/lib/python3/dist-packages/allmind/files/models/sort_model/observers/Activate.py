# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from allmind.alfa.LoopSignalReceiver import AlfaLoopSignalReceiver
from allmind.files import Signals
from allmind.open_file_with.OpenFileWith import DeltaOpenFileWith
from gi.repository import Gio


class DeltaActivate(AlfaLoopSignalReceiver):

    __accept_signal__ = Signals.ACTIVATE

    def _move_directory(self, file_info):
        path = file_info.get_attribute_string("kusozako1::path")
        user_data = Signals.MOVE_DIRECTORY, path
        self._raise("delta > loop signal", user_data)

    def _dispatch(self, file_info):
        file_type = file_info.get_file_type()
        if file_type == Gio.FileType.DIRECTORY:
            self._move_directory(file_info)
        elif file_type == Gio.FileType.REGULAR:
            DeltaOpenFileWith.start(file_info)

    def _on_signal_received(self, index):
        model = self._enquiry("delta > sort model")
        if index >= len(model):
            print("ALLMIND [error] > index out of range.")
        else:
            file_info = model[index]
            self._dispatch(file_info)
