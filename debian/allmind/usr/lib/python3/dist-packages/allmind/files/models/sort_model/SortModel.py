# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from allmind.Entity import DeltaEntity
from allmind.files import Signals
from .sorters.Sorters import EchoSorters
from .observers.Observers import EchoObservers


class DeltaSortModel(Gtk.SortListModel, DeltaEntity):

    @classmethod
    def new_for_model(cls, parent, model):
        instance = cls(parent)
        instance.construct(model)
        return instance

    def _delta_call_add_sorter(self, sorter):
        self._sorter.append(sorter)

    def _delta_info_sort_model(self):
        return self

    def construct(self, model):
        self.set_model(model)

    def __init__(self, parent):
        self._parent = parent
        self._sorter = Gtk.MultiSorter()
        Gtk.SortListModel.__init__(self, sorter=self._sorter)
        EchoSorters(self)
        EchoObservers(self)
