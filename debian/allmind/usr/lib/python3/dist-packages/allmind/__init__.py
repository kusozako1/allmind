# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gi
import readline

gi.require_version('Gtk', '4.0')
readline.clear_history()
