# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from allmind.Entity import DeltaEntity
from allmind.files import Signals


class AlfaLoopSignalReceiver(DeltaEntity):

    __accept_signal__ = "define accept signal here."

    def _on_signal_received(self, param=None):
        raise NotImplementedError

    def receive_transmission(self, user_data):
        signal, index = user_data
        if signal != self.__accept_signal__:
            return
        self._on_signal_received(index)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register loop object", self)
