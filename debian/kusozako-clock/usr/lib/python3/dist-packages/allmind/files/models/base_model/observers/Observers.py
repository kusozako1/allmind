# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ModelsReady import DeltaModelsReady
from .MoveDirectory import DeltaMoveDirectory


class EchoObservers:

    def __init__(self, parent):
        DeltaModelsReady(parent)
        DeltaMoveDirectory(parent)
