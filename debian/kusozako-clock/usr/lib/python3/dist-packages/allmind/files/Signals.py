# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

MODELS_READY = "models-ready"                       # None
ACTIVATE = "activate"                               # single arg
MOVE_DIRECTORY = "move-directory"                   # path as str
SHOW_CURRENT_DIECTORY = "show-current-directory"    # None

# UNUSED
ENUMERATION_FINISHED = "enumeration-finished"       # gfile
