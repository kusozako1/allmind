# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from allmind.Entity import DeltaEntity
from allmind.files import Signals
from .base_model.BaseModel import DeltaBaseModel
from .filter_model.FilterModel import DeltaFilterModel
from .sort_model.SortModel import DeltaSortModel


class DeltaModels(DeltaEntity):

    def show_message(self):
        print("show message ?")
        user_data = Signals.SHOW_CURRENT_DIECTORY, None
        self._raise("delta > loop signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        self._base_model = DeltaBaseModel(self)
        filter_model = DeltaFilterModel.new_for_model(self, self._base_model)
        _ = DeltaSortModel.new_for_model(self, filter_model)
        user_data = Signals.MODELS_READY, None
        self._raise("delta > loop signal", user_data)
