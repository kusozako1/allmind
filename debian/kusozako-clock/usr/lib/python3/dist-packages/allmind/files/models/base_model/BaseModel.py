# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from allmind.Entity import DeltaEntity
from allmind.files import Signals
from .observers.Observers import EchoObservers


class DeltaBaseModel(Gio.ListStore, DeltaEntity):

    def _set_attribute(self, file_info):
        name = file_info.get_name()
        gfile = self._gfile.get_child(name)
        file_info.set_attribute_string("kusozako1::path", gfile.get_path())
        return file_info

    def _add_parent(self):
        parent_gfile = self._gfile.get_parent()
        if parent_gfile is None:
            return
        file_info = parent_gfile.query_info("*", 0)
        path = parent_gfile.get_path()
        file_info.set_attribute_string("kusozako1::path", path)
        file_info.set_display_name("..")
        self.append(file_info)

    def _delta_call_move_directory(self, path):
        self.remove_all()
        self._gfile = Gio.File.new_for_path(path)
        self._add_parent()
        for file_info in self._gfile.enumerate_children("*", 0):
            file_info = self._set_attribute(file_info)
            self.append(file_info)

    def __init__(self, parent):
        self._parent = parent
        Gio.ListStore.__init__(self, item_type=Gio.FileInfo)
        EchoObservers(self)
