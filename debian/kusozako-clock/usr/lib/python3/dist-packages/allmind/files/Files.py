# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from allmind.alfa.NonUniqueLoop import AlfaNonUniqueLoop
from .parser.Parser import DeltaParser
from .models.Models import DeltaModels


class DeltaFiles(AlfaNonUniqueLoop):

    __mode__ = "files mode"

    def _parse(self, response):
        self._parser.parse(response)

    def _show_model_message(self):
        self._models.show_message()

    def construct(self, param=None):
        self._parser = DeltaParser(self)
        self._models = DeltaModels(self)
