# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from allmind.files import Signals
from allmind.alfa.LoopSignalReceiver import AlfaLoopSignalReceiver


class DeltaModelsReady(AlfaLoopSignalReceiver):

    __accept_signal__ = Signals.MODELS_READY

    def _on_signal_received(self, path):
        path = GLib.get_home_dir()
        self._raise("delta > move directory", path)
