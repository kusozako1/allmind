# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from allmind.Entity import DeltaEntity
from allmind.files import Signals

COMMANDS = {
    "a": Signals.ACTIVATE,
    "activate": Signals.ACTIVATE,
}


class DeltaCommand(DeltaEntity):

    def _command_not_found(self):
        print("ALLMIND [error] > Sorry, but I can't recognize your command.")

    def _get_words(self, response):
        words = response.split(" ")
        if words[0].isdigit():
            words.insert(0, "activate")
        if words[0] in ("..", "../", "up"):
            return ["activate", "0"]
        return words

    def parse(self, response):
        words = self._get_words(response)
        command = COMMANDS.get(words.pop(0), None)
        if command is not None:
            self._raise("delta > command found", (command, words))
        else:
            self._command_not_found()

    def __init__(self, parent):
        self._parent = parent
