# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from allmind.Entity import DeltaEntity
from allmind.files import Signals
from .Command import DeltaCommand
from .Arguments import DeltaArguments


class DeltaParser(DeltaEntity):

    def _delta_call_command_found(self, user_data):
        command, rest_words = user_data
        self._arguments.parse(command, rest_words)

    def parse(self, response):
        self._command.parse(response)

    def __init__(self, parent):
        self._parent = parent
        self._command = DeltaCommand(self)
        self._arguments = DeltaArguments(self)
