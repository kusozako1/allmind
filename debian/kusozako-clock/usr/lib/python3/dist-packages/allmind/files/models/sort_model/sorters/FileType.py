# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from allmind.Entity import DeltaEntity


class DeltaFileType(Gtk.CustomSorter, DeltaEntity):

    def _sort_func(self, alfa, bravo, user_data=None):
        return bravo.get_file_type() - alfa.get_file_type()

    def __init__(self, parent):
        self._parent = parent
        Gtk.CustomSorter.__init__(self)
        self.set_sort_func(self._sort_func)
        self._raise("delta > add sorter", self)
