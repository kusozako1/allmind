# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from allmind.Entity import DeltaEntity
from allmind.files import Signals

ARGUMENT_QUERY = {
    Signals.ACTIVATE: "_get_first_path",
}


class DeltaArguments(DeltaEntity):

    def _show_error_message(self):
        print("ALLMIND [error] > argument may be invalid.")

    def _decode(self, word):
        if word.isdigit():
            return True, int(word)
        return False, None

    def _get_first_path(self, words):
        for word in words:
            return self._decode(word)
        return False, words

    def parse(self, command, rest_words):
        method_name = ARGUMENT_QUERY.get(command, None)
        if method_name is None:
            self._show_error_message()
        method = getattr(self, method_name)
        is_valid, args = method(rest_words)
        if is_valid:
            self._raise("delta > loop signal", (command, args))

    def __init__(self, parent):
        self._parent = parent
