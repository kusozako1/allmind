# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from allmind.Entity import DeltaEntity
from allmind.Transmitter import FoxtrotTransmitter


class AlfaNonUniqueLoop(DeltaEntity):

    __mode__ = "define mode name here"

    @classmethod
    def start(cls, param=None):
        instance = cls()
        instance.construct(param)
        return instance.loop()

    def _parse(self, response):
        raise NotImplementedError

    def _get_response(self):
        return input("ALLMIND [{}] > command ? : ".format(self.__mode__))

    def _parse_common_commands(self, response):
        if response in ("quit", "q", "bye"):
            return False
        if response in ("back to home", ""):
            return True
        return None

    def loop(self):
        while True:
            self._show_model_message()
            response = self._get_response()
            hold_main_loop = self._parse_common_commands(response)
            if hold_main_loop is not None:
                return hold_main_loop
            self._parse(response)

    def construct(self, param=None):
        pass

    def _delta_call_register_loop_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_loop_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def __init__(self):
        self._parent = None
        self._transmitter = FoxtrotTransmitter()
