# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from .files.Files import DeltaFiles


class DeltaMainLoop:

    @classmethod
    def start(cls):
        instance = cls()
        instance.loop()

    def _loop(self):
        response = input("ALLMIND > I'm at your service. command ? : ")
        if response in ("quit", "q", "bye"):
            return False
        if response in ("files", "file", "f"):
            return DeltaFiles.start()
        return True

    def loop(self):
        print("ALLMIND > welcome Back, {}.".format(GLib.get_user_name()))
        hold = True
        while hold:
            hold = self._loop()
        print("ALLMIND > bye...")
