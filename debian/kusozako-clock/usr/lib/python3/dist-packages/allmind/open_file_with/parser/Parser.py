# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from allmind.Entity import DeltaEntity
from allmind.open_file_with import Signals


class DeltaParser(DeltaEntity):

    def _decode(self, word):
        if word.isdigit():
            return True, int(word)
        return False, None

    def _get_first_path(self, words):
        for word in words:
            return self._decode(word)
        return False, words

    def parse(self, response):
        is_valid, index = self._get_first_path(response)
        if not is_valid:
            return
        user_data = Signals.LAUNCH_INDEX, index
        self._raise("delta > loop signal", user_data)

    def __init__(self, parent):
        self._parent = parent
