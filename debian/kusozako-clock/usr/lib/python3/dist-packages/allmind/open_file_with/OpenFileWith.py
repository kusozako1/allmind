# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

# from allmind.Entity import DeltaEntity
# from allmind.Transmitter import FoxtrotTransmitter
from allmind.alfa.NonUniqueLoop import AlfaNonUniqueLoop
from .model.Model import DeltaModel
from .parser.Parser import DeltaParser

TEMPLATE = "ALLMIND [{}] > select app to open file : "


class DeltaOpenFileWith(AlfaNonUniqueLoop):

    __mode__ = "select app"

    def _get_response(self):
        response = input(TEMPLATE.format(self.__mode__))
        if response in ("back", "quit", "q", ""):
            return False, ""
        return None, response

    def _delta_call_release_loop(self):
        self._hold = False

    def loop(self):
        while self._hold:
            hold, response = self._get_response()
            if hold is not None:
                return hold
            self._parser.parse(response)

    def construct(self, file_info):
        self._hold = True
        self._parser = DeltaParser(self)
        self._model = DeltaModel(self)
        self._model.set_file_info(file_info)
