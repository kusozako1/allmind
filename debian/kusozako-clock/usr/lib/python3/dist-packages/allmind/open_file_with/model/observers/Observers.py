# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .LaunchIndex import DeltaLaunchIndex


class EchoObservers:

    def __init__(self, parent):
        DeltaLaunchIndex(parent)
