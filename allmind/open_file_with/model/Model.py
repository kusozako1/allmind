# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from allmind.Entity import DeltaEntity
from .observers.Observers import EchoObservers


class DeltaModel(Gio.ListStore, DeltaEntity):

    def _show(self, index, desktop_app_info):
        name = desktop_app_info.get_name()
        command_line = desktop_app_info.get_commandline()
        print("{} : {} ({})".format(index, name, command_line))

    def _delta_info_file_info(self):
        return self._file_info

    def _delta_info_model(self):
        return self

    def show_model_message(self):
        pass

    def set_file_info(self, file_info):
        self._file_info = file_info
        content_type = file_info.get_content_type()
        index = 0
        for desktop_app_info in Gio.AppInfo.get_all_for_type(content_type):
            self.append(desktop_app_info)
            self._show(index, desktop_app_info)
            index += 1

    def __init__(self, parent):
        self._parent = parent
        Gio.ListStore.__init__(self, item_type=Gio.DesktopAppInfo)
        EchoObservers(self)
