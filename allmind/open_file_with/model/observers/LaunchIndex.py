# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from allmind.open_file_with import Signals
from allmind.alfa.LoopSignalReceiver import AlfaLoopSignalReceiver


class DeltaLaunchIndex(AlfaLoopSignalReceiver):

    __accept_signal__ = Signals.LAUNCH_INDEX

    def _launch(self, model):
        desktop_app_info = model[index]
        file_info = self._enquiry("delta > file info")
        path = file_info.get_attribute_string("kusozako1::path")
        gfile = Gio.File.new_for_path(path)
        context = Gio.AppLaunchContext.new()
        launched = desktop_app_info.launch([gfile], context)
        if launched:
            self._raise("delta > release loop")

    def _on_signal_received(self, index):
        model = self._enquiry("delta > model")
        if index >= len(model):
            return
        else:
            self._launch(model)
