# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FileType import DeltaFileType
from .DisplayName import DeltaDisplayName


class EchoSorters:

    def __init__(self, parent):
        DeltaFileType(parent)
        DeltaDisplayName(parent)
