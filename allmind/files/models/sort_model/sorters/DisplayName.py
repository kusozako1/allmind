# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from allmind.Entity import DeltaEntity


class DeltaDisplayName(Gtk.CustomSorter, DeltaEntity):

    def _get_collate_key(self, file_info):
        display_name = file_info.get_display_name()
        return GLib.utf8_collate_key_for_filename(display_name, -1)

    def _sort_func(self, alfa, bravo, user_data=None):
        alfa_collate_key = self._get_collate_key(alfa)
        bravo_collate_key = self._get_collate_key(bravo)
        return 1 if alfa_collate_key >= bravo_collate_key else -1

    def __init__(self, parent):
        self._parent = parent
        Gtk.CustomSorter.__init__(self)
        self.set_sort_func(self._sort_func)
        self._raise("delta > add sorter", self)
