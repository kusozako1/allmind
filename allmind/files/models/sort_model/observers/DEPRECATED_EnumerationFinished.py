# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from allmind.alfa.LoopSignalReceiver import AlfaLoopSignalReceiver
from allmind.files import Signals


class DeltaEnumerationFinished(AlfaLoopSignalReceiver):

    __accept_signal__ = Signals.ENUMERATION_FINISHED

    def _on_signal_received(self, param=None):
        index = 0
        for file_info in self._enquiry("delta > sort model"):
            print("{} : {}".format(index, file_info.get_display_name()))
            index += 1
