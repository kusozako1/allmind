# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ShowHidden import DeltaShowHidden


class EchoFilters:

    def __init__(self, parent):
        DeltaShowHidden(parent)
